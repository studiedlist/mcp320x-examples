#![no_std]
#![no_main]

use esp_backtrace as _;
use esp_println::println;
use hal::{
    clock::ClockControl,
    peripherals::Peripherals,
    prelude::*,
    spi::{Spi, SpiMode},
    Delay, IO,
};
use mcp320x::{Channels8, Mcp320X};

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let mut system = peripherals.DPORT.split();
    let clocks = ClockControl::max(system.clock_control).freeze();
    let mut delay = Delay::new(&clocks);
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let cs = io.pins.gpio16.into_push_pull_output();
    let miso = io.pins.gpio18.into_push_pull_output();
    let mosi = io.pins.gpio17.into_push_pull_output();
    let clk = io.pins.gpio19.into_push_pull_output();

    let spi = Spi::new_no_cs(
        peripherals.SPI2,
        clk,
        mosi,
        miso,
        1600u32.kHz(),
        SpiMode::Mode0,
        &mut system.peripheral_clock_control,
        &clocks,
    );

    let mut mcp3208 = Mcp320X::new(spi, cs);


    loop {
        delay.delay_ms(500u32);
        println!();
        for c in Channels8::list() {
            println!("channel #{}: {}", c as u8, mcp3208.read_channel(c).unwrap());
        }
    }
}
